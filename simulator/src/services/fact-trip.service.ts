import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { AmqpConnection } from '@golevelup/nestjs-rabbitmq';
import { EventTripDto } from "src/repositories/dto/event-trip.dto";
import { TripStatus } from "src/repositories/dto/trip-helper";
import { FactTrip, FactTripDocument } from "src/repositories/fact-trip.entity";
import { RABBITMQ_FACT_KEY, RABBITMQ_SIMULATOR_EXCHANGE, RABBITMQ_STATUS_KEY } from "src/utils/constants";
import { StatusTripDto } from "src/repositories/dto/status-trip.dto";
import { HeartbeatDto } from "src/repositories/dto/heartbeat.dto";

@Injectable()
export class FactTripsService {
    constructor(
        @InjectModel(FactTrip.name) private factTripModel: Model<FactTripDocument>,
        private readonly amqpConnection: AmqpConnection ) { }

    async create(eventTripDto: EventTripDto) {
        console.log(`recieved trip: ${JSON.stringify(eventTripDto, null, '   ')}`);
        await this.factTripModel.create({
            ...eventTripDto, 
            status: TripStatus.OnItsWay,
            _id: eventTripDto.id
        });

        let factTrip = await this.findById(eventTripDto.id);
        await this.amqpConnection.publish(
            RABBITMQ_SIMULATOR_EXCHANGE,
            RABBITMQ_STATUS_KEY,
            new StatusTripDto(eventTripDto.id, factTrip.status)
          );

        factTrip.currentState = factTrip.startState();
        await this.amqpConnection.publish(
            RABBITMQ_SIMULATOR_EXCHANGE,
            RABBITMQ_FACT_KEY,
            new HeartbeatDto(factTrip)
          );
        return factTrip;
    }

    findById(id: string): Promise<FactTrip> {
        return this.factTripModel.findById(id);
    }

    findActive(): Promise<FactTrip[]> {
        return this.factTripModel.find({status: TripStatus.OnItsWay}, );
    }

    async update(factTrip: FactTrip) {
        factTrip.move();
        let result = await this.factTripModel.findByIdAndUpdate(factTrip._id, factTrip);
        await this.amqpConnection.publish(
            RABBITMQ_SIMULATOR_EXCHANGE,
            RABBITMQ_FACT_KEY,
            new HeartbeatDto(factTrip)
          );

        if (factTrip.status === TripStatus.Complete) {
            await this.amqpConnection.publish(
                RABBITMQ_SIMULATOR_EXCHANGE,
                RABBITMQ_STATUS_KEY,
                new StatusTripDto(factTrip._id.toString(), factTrip.status)
              ); 
        }
        return result;        
    }

    async generateHeardBeats() {
        const activeTrips = await this.findActive();
        for (const trip of activeTrips) {
            this.update(trip);
        }
    }
}