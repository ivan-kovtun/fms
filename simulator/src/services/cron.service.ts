import { Injectable, Logger } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { TIME_QUANT } from 'src/utils/constants';
import { FactTripsService } from './fact-trip.service';

@Injectable()
export class CronService {
    constructor(private factTripService: FactTripsService) { }

    @Cron(`*/${TIME_QUANT} * * * * *`)
    handleCron() {
        this.factTripService.generateHeardBeats();
    }
}