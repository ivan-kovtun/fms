import { Nack, RabbitRPC } from '@golevelup/nestjs-rabbitmq';
import { Injectable } from '@nestjs/common';
import { EventTripDto } from 'src/repositories/dto/event-trip.dto';

import { 
    RABBITMQ_TRIP_QUEUE, 
    RABBITMQ_TRIP_EXCHANGE, 
    RABBITMQ_TRIP_KEY,
} from '../utils/constants';
import { FactTripsService } from './fact-trip.service';


@Injectable()
export class TripEventsService {
    constructor(private factTripsService: FactTripsService) { }

    @RabbitRPC({
        exchange: RABBITMQ_TRIP_EXCHANGE,
        routingKey: RABBITMQ_TRIP_KEY,
        queue: RABBITMQ_TRIP_QUEUE,
    })
    public async rpcHandler( msg: EventTripDto ) {
        try {
            await this.factTripsService.create(msg);
            return new Nack();            
        } catch (error) {
            console.error(error);
            return new Nack(true);
        }
    }
}