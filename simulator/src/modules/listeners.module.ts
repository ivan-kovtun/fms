import { RabbitMQModule } from "@golevelup/nestjs-rabbitmq";
import { Module } from "@nestjs/common/decorators/modules/module.decorator";
import { ConfigModule, ConfigService } from "@nestjs/config";
import { MongooseModule } from "@nestjs/mongoose";
import { CronService } from "src/services/cron.service";
import { FactTripsService } from "src/services/fact-trip.service";
import { TripEventsService } from "src/services/trip-events.service";
import { FactTrip, FactTripSchema } from "../repositories/fact-trip.entity";
import { RABBITMQ_SIMULATOR_EXCHANGE, RABBITMQ_SIMULATOR_EXCHANGE_TYPE, RABBITMQ_URI } from "../utils/constants";

@Module({
    imports: [
        MongooseModule.forFeature([
            { name: FactTrip.name, schema: FactTripSchema }
        ]),
        ConfigModule,
        RabbitMQModule.forRootAsync(RabbitMQModule, {
            imports: [ConfigModule.forRoot()],
            useFactory: async (configService: ConfigService) => {
              let options = ({
                exchanges: [
                  {
                    name: RABBITMQ_SIMULATOR_EXCHANGE,
                    type: RABBITMQ_SIMULATOR_EXCHANGE_TYPE,
                  },
                ],
                uri: configService.get<string>(RABBITMQ_URI),
                connectionInitOptions: { wait: false },
              });
              return options;
            },
            inject: [ConfigService]
          })
    ],
    providers: [TripEventsService, FactTripsService, CronService],
})
export class ListenersModule { }