import { DriverDto } from "./driver.dto";

export class CarDto {
    id: string
    ref: string;
    model: string;
    maxSpeed: number;
    year: string;
    driver: DriverDto;

    constructor(car: CarDto) {
        Object.assign(this, car);
        this.driver = new DriverDto(car.driver);  
    }
}
