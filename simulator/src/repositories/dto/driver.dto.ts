export class DriverDto {
    id: string
    firstName: string;
    lastName: string;
    risk: number;
    licenseId: string;

    constructor(driver: any) {
        Object.assign(this, driver); 
    }
}
