import { Point } from './trip-helper';

export class StateDto {
    position: Point;
    speed: number;
    time: Date;
}