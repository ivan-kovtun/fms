import { CarDto } from './car.dto';
import { Point, TripStatus } from './trip-helper';


export class EventTripDto {
    id: string;
    origin: Point;
    destination: Point;
    car: CarDto;
    status: TripStatus;
    started: Date;
    finished: Date;

    constructor(trip: any){
        this.id = trip._id.toString();
        this.status = trip.status;
        this.started = trip.started;
        this.finished = trip.finished;
        this.origin = new Point;
        Object.assign(this.origin, trip.origin);
        this.destination = new Point;
        Object.assign(this.destination, trip.destination);
        this.car = new CarDto(trip.car);
    }
}