import { PartialType } from '@nestjs/mapped-types';
import { FactTrip } from '../fact-trip.entity';
import { CarDto } from './car.dto';
import { DriverDto } from './driver.dto';
import { StateDto } from './state.dto';

class Driver {
    id: string;
    firstName: string;
    lastName: string;
    licenseId: string;
    constructor(driver: DriverDto) {
        this.id = driver.id;
        this.firstName = driver.firstName;
        this.lastName = driver.lastName;
        this.licenseId = driver.licenseId;
    }
}

class Car {
    id: string;
    ref: string;
    model: string;
    driver: Driver;
    constructor(car: CarDto) {
        this.id = car.id;
        this.ref = car.ref;
        this.model = car.model;
        this.driver = new Driver(car.driver);
    }
}

export class HeartbeatDto extends PartialType(StateDto) {
    car: Car;
    constructor(factTrip: FactTrip) {
        super();
        this.car = new Car(factTrip.car);
        this.position = {...factTrip.currentState.position};
        this.speed = factTrip.currentState.speed;
        this.time = factTrip.currentState.time;
    }
}