import { TripStatus } from './trip-helper';

export class StatusTripDto {
    status: TripStatus;
    id: string;
    constructor(id: string, status: TripStatus) {
        this.id = id;
        this.status = status;
    }
}