export class Point {
    latitude: number;
    longitude: number;
}

export enum TripStatus {
    Waiting = 'waiting',
    OnItsWay = 'onItsWay',
    Complete = 'complete',
    Failed = 'failed'
}