import * as mongoose from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Point, TripStatus } from './dto/trip-helper';
import { CarDto } from './dto/car.dto';
import { StateDto } from './dto/state.dto';
import { randomInt } from 'crypto';
import { DESTINATION_ACCURACY, TIME_QUANT, TIME_RATE } from 'src/utils/constants';
import { getRhumbLineBearing, computeDestinationPoint, getDistance } from 'geolib';

export type FactTripDocument = mongoose.HydratedDocument<FactTrip>;

@Schema()
export class FactTrip {

    _id: mongoose.ObjectId;

    @Prop({ required: true })
    readonly origin: Point;

    @Prop({ required: true })
    readonly destination: Point;

    @Prop({ required: true })
    readonly car: CarDto;

    @Prop({ required: true })
    status: TripStatus;

    @Prop({ required: true })
    readonly started: Date;

    @Prop()
    finished: Date;

    @Prop()
    currentState: StateDto;

    @Prop()
    history: StateDto[];

    public move: Function;
    public startState: Function;
    private setSpeed: Function;
    private setTime: Function;
    private getDistance: Function;
    private getPeriod: Function;    
    private getBearing: Function;
    private previousState: Function;
    private metPerSec: Function;
}

export const FactTripSchema = SchemaFactory.createForClass(FactTrip);

FactTripSchema.methods.move = function() {

    if (this.currentState) {
        this.history.push(this.currentState);
    } else {
        this.history.push(this.startState());
    }
    this.currentState = new StateDto;
    this.setSpeed();
    this.setTime();
    const bearing = this.getBearing();

    const totalDistance = getDistance(this.previousState().position, this.destination);

    let distance = this.getDistance();
    distance = totalDistance < distance ? totalDistance : distance;

    const newPosition = computeDestinationPoint(
        this.previousState().position,
        distance,
        bearing
    );
    
    const finalDistance = getDistance(newPosition, this.destination);
    if (finalDistance < parseFloat(process.env[DESTINATION_ACCURACY])) {
        this.status = TripStatus.Complete;
    }

    const moveDistance = getDistance(this.previousState().position, newPosition);

    this.currentState.position = { ...newPosition };
    console.log(`The car ${[this.car.ref, this.car.model]} 
        is moving from ${JSON.stringify(this.previousState().position)} 
        to ${JSON.stringify(this.currentState.position)} 
        (bearing ${bearing} speed ${this.currentState.speed} km/h total distance ${totalDistance} m
        move distance ${moveDistance} m final distance ${finalDistance} m for ${this.getPeriod()} sec)`);
};

FactTripSchema.methods.startState = function(): StateDto {
    let initialState = new StateDto;
    initialState.position = { ...this.origin };
    initialState.speed = 0;
    initialState.time = this.started;
    return initialState;
};

FactTripSchema.methods.setSpeed = function(): void  {
    let baseSpeed = this.car.maxSpeed * this.car.driver.risk / 100;
    this.currentState.speed = randomInt(baseSpeed);
};

FactTripSchema.methods.setTime = function() {
    const timeRate = parseInt(process.env[TIME_RATE]);
    if (timeRate === 0) {
        this.currentState.time = new Date();
    } else {
        const prevTime = this.previousState().time;
        const newTimestamp = prevTime.getTime() + timeRate * TIME_QUANT * 1000;
        this.currentState.time= new Date(newTimestamp);
    } 
};

FactTripSchema.methods.getPeriod = function() {

    const period = this.currentState.time.getTime() - this.previousState().time.getTime();
    return period / 1000;
};

FactTripSchema.methods.getDistance = function() {
    const period = this.getPeriod();
    return this.metPerSec(this.currentState.speed) * period;
};

FactTripSchema.methods.getBearing = function()  {
    return getRhumbLineBearing(this.previousState().position, this.destination);
};

FactTripSchema.methods.previousState = function() : StateDto {
    return this.history.slice(-1)[0];
};

FactTripSchema.methods.metPerSec = function(kmPerHour: number) {
    const secInHour = 3600;
    const kilo = 1000;
    return kilo * kmPerHour / secInHour;
};