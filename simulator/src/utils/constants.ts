export const MONGO_PROVIDER = 'DATABASE_CONNECTION';
export const MONGODB_URI = 'MONGODB_URI';
export const MONDODB_NAME = 'MONDODB_NAME';
export const MONGODB_ERROR = 'MONGODB_ERROR';

export const RABBITMQ_URI = 'RABBITMQ_URI';
export const RABBITMQ_TRIP_EXCHANGE = 'fms.masterData.trip';
export const RABBITMQ_TRIP_QUEUE = 'fms.trip.events';
export const RABBITMQ_TRIP_KEY = 'fms.trip.events';
export const RABBITMQ_SIMULATOR_EXCHANGE = 'fms.simulator';
export const RABBITMQ_SIMULATOR_EXCHANGE_TYPE = 'topic';
export const RABBITMQ_STATUS_QUEUE = 'fms.trip.statuses';
export const RABBITMQ_STATUS_KEY = 'fms.trip.statuses';
export const RABBITMQ_FACT_QUEUE = 'fms.trip.heartbears';
export const RABBITMQ_FACT_KEY = 'fms.trip.heartbears';

export const TIME_RATE = 'TIME_RATE';
export const TIME_QUANT = 5;
export const DESTINATION_ACCURACY = 'DESTINATION_ACCURACY';


