import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { DriversPointsModule } from './modules/driversPoints.module';
import { MONDODB_NAME, MONGODB_URI } from './utils/constants';

@Module({
  imports: [ConfigModule.forRoot({
    isGlobal: true,
  }),
  MongooseModule.forRootAsync({
    imports: [ConfigModule],
    useFactory: async (configService: ConfigService) => ({
      uri: configService.get<string>(MONGODB_URI),
      dbName: configService.get<string>(MONDODB_NAME)
    }),
    inject: [ConfigService]
  }),
  DriversPointsModule],
  controllers: [],
  providers: [],
})
export class AppModule {}
