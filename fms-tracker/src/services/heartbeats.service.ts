import { Nack, RabbitRPC } from '@golevelup/nestjs-rabbitmq';
import { Injectable } from '@nestjs/common';
import { HeartbeatDto } from 'src/repositories/heartbeat.dto';

import { 
    RABBITMQ_FACT_QUEUE, 
    RABBITMQ_SIMULATOR_EXCHANGE, 
    RABBITMQ_FACT_KEY,
} from '../utils/constants';
import { DriversPointsService } from './driversPoints.service';

@Injectable()
export class HertbeatsService {
    constructor(private driversPointsService: DriversPointsService) { }

    @RabbitRPC({
        exchange: RABBITMQ_SIMULATOR_EXCHANGE,
        routingKey: RABBITMQ_FACT_KEY,
        queue: RABBITMQ_FACT_QUEUE,
    })
    public async rpcHandler( msg: HeartbeatDto ) {
        try {
            await this.driversPointsService.registerHeartbeat(msg);
            return new Nack();            
        } catch (error) {
            console.error(error);
            return new Nack(true);
        }
    }
}