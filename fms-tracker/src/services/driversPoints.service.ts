import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { getDistance } from 'geolib';
import { Model } from 'mongoose';
import { DriverPoints, DriverPointsDocument } from 'src/repositories/driversPoints.entity';
import { HeartbeatDto } from 'src/repositories/heartbeat.dto';
import { Heartbeat } from 'src/repositories/heartbeat.helper';
import { PENALTY_SPEED_OVER_100, PENALTY_SPEED_OVER_60, PENALTY_SPEED_OVER_80, SPEED_100, SPEED_60, SPEED_80 } from 'src/utils/constants';

@Injectable()
export class DriversPointsService {
    constructor(@InjectModel(DriverPoints.name) private driverPointsModel: Model<DriverPointsDocument>) { }

    create(heartbeat: HeartbeatDto): Promise<DriverPoints> {
        return this.driverPointsModel.create({
            _id: heartbeat.car.driver.id,
            firstName: heartbeat.car.driver.firstName,
            lastName: heartbeat.car.driver.lastName,
            licenseId: heartbeat.car.driver.licenseId,
            heartbeats: [{
                carRef: heartbeat.car.ref,
                carId: heartbeat.car.id,
                position: {...heartbeat.position},
                speed: heartbeat.speed,
                time: heartbeat.time
            }],
            penaltyMap: new Map(),
            penaltyPoints: 0,
        });
    }

    findById(id: string) {
        return this.driverPointsModel.findById(id).exec();
    }

    findAll(filter = undefined) {
        return this.driverPointsModel.find(filter);
    }

    async registerHeartbeat(heartbeat: HeartbeatDto) {
        let driverReg = await this.findById(heartbeat.car.driver.id);
        if (driverReg) {
            const previousHeartbeat = driverReg.heartbeats.slice(-1)[0];
            const currentHeartbeat = {
                carRef: heartbeat.car.ref,
                carId: heartbeat.car.id,
                position: {...heartbeat.position},
                speed: heartbeat.speed,
                time: heartbeat.time
            };
            this.calculatePenalty(driverReg, previousHeartbeat, currentHeartbeat);
            driverReg.heartbeats.push(currentHeartbeat);
            await driverReg.save();
        } else {
            this.create(heartbeat);   
        }
    }

    private calculatePenalty(reg: DriverPoints, prev: Heartbeat, cur: Heartbeat) {
        if ( cur.speed < SPEED_60 ) return;
        if ( cur.carId !== prev.carId) return;
        const kilo = 1000;
        const secInHour = 60*60;
        let distance = getDistance(prev.position, cur.position);
        console.log(`DISTANCE_OVER_60 is [${distance}] m (between ${JSON.stringify(prev.position)} and ${JSON.stringify(cur.position)})`);
        distance = distance / kilo;
        const prevDate = new Date(prev.time);
        const curDate = new Date(cur.time);
        const period = (curDate.getTime() 
                        - prevDate.getTime())/kilo/secInHour;
        let speed = cur.speed;
        if ( period > 1 ) {
            speed = distance / period;
        }
        if ( speed < SPEED_80 ) {
            let oldDistance: number = reg.penaltyMap.get(PENALTY_SPEED_OVER_60)||0;
            reg.penaltyMap.set(PENALTY_SPEED_OVER_60, oldDistance + distance);
        } else if ( speed < SPEED_100 ) {
            let oldDistance: number = reg.penaltyMap.get(PENALTY_SPEED_OVER_80)||0;
            reg.penaltyMap.set(PENALTY_SPEED_OVER_80, oldDistance + distance);
        } else if ( speed >= SPEED_100 ) {
            let oldDistance: number = reg.penaltyMap.get(PENALTY_SPEED_OVER_100)||0;
            reg.penaltyMap.set(PENALTY_SPEED_OVER_100, oldDistance + distance);
        }
        console.log(`PENALTY_SPEED_OVER_60 is [${reg.penaltyMap.get(PENALTY_SPEED_OVER_60)}]`);
        console.log(`PENALTY_SPEED_OVER_80 is [${reg.penaltyMap.get(PENALTY_SPEED_OVER_80)}]`);
        console.log(`PENALTY_SPEED_OVER_100 is [${reg.penaltyMap.get(PENALTY_SPEED_OVER_100)}]`);        
        reg.penaltyPoints = 0;
        reg.penaltyMap.forEach((value, key) => {
            reg.penaltyPoints +=  parseInt(key) * Math.round(value);   
        })
    }
}
