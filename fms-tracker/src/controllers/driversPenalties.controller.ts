import { Controller, Get, Post, Body, Patch, Param, Delete, Request } from '@nestjs/common';
import { DriversPointsService } from "src/services/driversPoints.service";
import { urlParamsToFilter } from 'src/utils/urlParamsToFilter';

@Controller('drivers-penalties')
export class DriversPenaltiesController {
    constructor(private readonly driversPointsService: DriversPointsService) { }

    @Get()
    async findAll(@Request() req) {
        let filter = urlParamsToFilter(req);
        return await (await this.driversPointsService.findAll(filter)).map(driverPoints => ({
            id: driverPoints._id.toString(),
            firstName: driverPoints.firstName,
            lastName: driverPoints.lastName,
            licenseId: driverPoints.licenseId,
            penaltyPoints: driverPoints.penaltyPoints,
        }));
    }

    @Get(':id')
    async findId(@Param('id') id: string) {
        let driverPoints = await this.driversPointsService.findById(id);
        return {
            id: driverPoints._id.toString(),
            firstName: driverPoints.firstName,
            lastName: driverPoints.lastName,
            licenseId: driverPoints.licenseId,
            penaltyPoints: driverPoints.penaltyPoints,
            penaltyMap: driverPoints.penaltyMap
        };
    }

}