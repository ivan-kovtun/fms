import { RabbitMQModule } from "@golevelup/nestjs-rabbitmq";
import { Module } from "@nestjs/common/decorators/modules/module.decorator";
import { ConfigModule, ConfigService } from "@nestjs/config";
import { MongooseModule } from "@nestjs/mongoose";
import { DriversPenaltiesController } from "src/controllers/driversPenalties.controller";
import { DriverPoints, DriverPointsSchema } from "src/repositories/driversPoints.entity";
import { DriversPointsService } from "src/services/driversPoints.service";
import { HertbeatsService } from "src/services/heartbeats.service";

import { RABBITMQ_SIMULATOR_EXCHANGE, RABBITMQ_SIMULATOR_EXCHANGE_TYPE, RABBITMQ_URI } from "../utils/constants";

@Module({
    imports: [
        MongooseModule.forFeature([
            { name: DriverPoints.name, schema: DriverPointsSchema }
        ]),
        ConfigModule,
        RabbitMQModule.forRootAsync(RabbitMQModule, {
            imports: [ConfigModule.forRoot()],
            useFactory: async (configService: ConfigService) => {
              let options = ({
                exchanges: [
                  {
                    name: RABBITMQ_SIMULATOR_EXCHANGE,
                    type: RABBITMQ_SIMULATOR_EXCHANGE_TYPE,
                  },
                ],
                uri: configService.get<string>(RABBITMQ_URI),
                connectionInitOptions: { wait: false },
              });
              return options;
            },
            inject: [ConfigService]
          })
    ],
    providers: [HertbeatsService, DriversPointsService],
    controllers: [DriversPenaltiesController]
})
export class DriversPointsModule { }