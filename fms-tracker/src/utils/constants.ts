export const REST_CONNECTION_PORT = 'REST_CONNECTION_PORT';

export const MONGO_PROVIDER = 'DATABASE_CONNECTION';
export const MONGODB_URI = 'MONGODB_URI';
export const MONDODB_NAME = 'MONDODB_NAME';
export const MONGODB_ERROR = 'MONGODB_ERROR';

export const RABBITMQ_URI = 'RABBITMQ_URI';
export const RABBITMQ_SIMULATOR_EXCHANGE = 'fms.simulator';
export const RABBITMQ_SIMULATOR_EXCHANGE_TYPE = 'topic';
export const RABBITMQ_FACT_QUEUE = 'fms.trip.heartbears';
export const RABBITMQ_FACT_KEY = 'fms.trip.heartbears';

export const SPEED_60 = 60;
export const SPEED_80 = 80;
export const SPEED_100 = 100;

export const PENALTY_SPEED_OVER_60 = '1';
export const PENALTY_SPEED_OVER_80 = '2';
export const PENALTY_SPEED_OVER_100 = '5';





