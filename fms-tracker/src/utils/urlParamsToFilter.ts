export function urlParamsToFilter (req: any) {
    const searchParams = new URLSearchParams(req.url.includes('?') ? req.url.substring(req.url.indexOf('?')) : '');
    let filter = {};
    searchParams.forEach((value, name) => {
        filter[name] = value;
    });
    return filter;
}