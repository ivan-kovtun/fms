import * as mongoose from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Heartbeat } from './heartbeat.helper';


export type DriverPointsDocument = mongoose.HydratedDocument<DriverPoints>;


@Schema()
export class DriverPoints {

    _id: mongoose.ObjectId;
    
    @Prop({ required: true })
    readonly firstName: string;

    @Prop({ required: true })
    readonly lastName: string;

    @Prop({ required: true })
    readonly licenseId: string;

    @Prop()
    readonly heartbeats: Heartbeat[];

    @Prop()
    penaltyMap: Map<string, number>;

    @Prop()
    penaltyPoints: number;
}

export const DriverPointsSchema = SchemaFactory.createForClass(DriverPoints);