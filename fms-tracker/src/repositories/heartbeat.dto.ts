import { PartialType } from '@nestjs/mapped-types';

class Driver {
    id: string;
    firstName: string;
    lastName: string;
    licenseId: string;
}

class Car {
    id: string;
    ref: string;
    model: string;
    driver: Driver;
}

export class Point {
    latitude: number;
    longitude: number;
}

export class HeartbeatDto {
    car: Car;
    position: Point;
    speed: number;
    time: Date;
}