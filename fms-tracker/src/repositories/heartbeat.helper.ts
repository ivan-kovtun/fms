import { Point } from './heartbeat.dto';

export class Heartbeat {
    carRef: string;
    carId: string;
    position: Point;
    speed: number;
    time: Date;
}
