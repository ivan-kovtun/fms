conn = new Mongo();
//db = conn.getDB("root-db");


db1 = conn.getDB('fms-master-data');
db1.cars.createIndex({ "ref": 1 }, { unique: true });
db1.drivers.createIndex({ "licenseId": 1 }, { unique: true });

db2 = conn.getDB('fms-tracker');
db2.drivers.createIndex({ "driver": 1 });

db3 = conn.getDB('fms-simulator');
db3.trips.createIndex({ "status": 1 });


