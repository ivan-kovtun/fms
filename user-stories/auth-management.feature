# OPA can be implemented in order to apply common policies for different components
# When we talk about a certain component it's easier to realise RBAC/ABAC to this particular service

Feature: Authorization management
    As a Security Engineer 
    I want to be sure that access to fms components satisfies current policies.

    # Security Engineer set component specific policies
    # General policy
    Scenario: Security Engineer set GWT auth policy for all FMS APIs
        When the Security Engineer set Authorization policy to fms-master-data API that requires use of GWT guard
        Then OPA allow only requests that use GWT auth

    Scenario: FMS data manager must be able to CRUD master data
        When the FMS data manager requests the master data with GWT auth
        Then request to make CRUD operations is allowed 

    Scenario: FMS data manager must be denied to CRUD operations
        When the FMS data manager requests the master data with auth except GWT
        Then request to make CRUD operations is denied


    Scenario: Security Engineer set advanced ABAC policy for Tracker/Penalty API
        When the Security Engineer set Authorization policy to fms-master-data API that requres use of GWT guard
        Then OPA updates requests in dependence on who (role/person) issue this request

    Scenario: Driver must be able to see the penalty points of all drivers
        When the driver requests the list of drivers and their penalties w/o additional parameters
        Then the Penalty API returns only basic info without license Ids, Ids, Penalty per type map 

    Scenario: Driver must not be able to see detailed data of other people
        When the driver requests the list of drivers and penalties with the projection of license Ids, Ids, Penalty per type map
        Then the Penalty API returns "access denied"

    Scenario: Driver must be able to see his own detailed penalty points
        When the driver requests the his own penalties with the projection of license Ids, Ids, Penalty per type map
        Then the Penalty API returns detailed data

    Scenario: Manager must be able to see drivers detailed penalty points
        When the manager requests the driver's data with the projection of license Ids, Ids, Penalty per type map
        Then the Penalty API returns detailed penalty data