import { AmqpConnection } from '@golevelup/nestjs-rabbitmq';
import { HttpStatus, Injectable } from '@nestjs/common';
import { HttpException } from '@nestjs/common/exceptions';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { CarsService } from 'src/services/cars.service';
import { CreateTripDto } from '../repositories/trips/dto/create-trip.dto';
import { EventTripDto } from '../repositories/trips/dto/messages/event-trip.dto';
import { TripStatus } from '../repositories/trips/dto/trip-helper';
import { UpdateTripDto } from '../repositories/trips/dto/update-trip.dto';
import { Trip } from '../repositories/trips/entities/trip.entity';
import { 
  RABBITMQ_ERROR_TRIP_NOT_FOUND, 
  RABBITMQ_TRIP_EXCHANGE, 
  RABBITMQ_TRIP_KEY 
} from 'src/utils/constants';

@Injectable()
export class TripsService {
  constructor(
    @InjectModel(Trip.name) private tripModel: Model<Document>,
    private carsService: CarsService,
    private readonly amqpConnection: AmqpConnection) { }

  async create(createTripDto: CreateTripDto) {
    let car = await this.carsService.findOne(createTripDto.car);
    if (!car) {
      throw new HttpException("This car is not found", HttpStatus.NOT_FOUND)
    }
    if (!car.get('driver')) {
      throw new HttpException("You should assign a driver to this car in order to use it for trip", HttpStatus.BAD_REQUEST)
    }

    let tripsOnWay = await this.tripModel.find({
      car: new Types.ObjectId(createTripDto.car),
      status: {
        $in: TripsService.activeStatuses()
      }
    });
    if (tripsOnWay.length) {
      throw new HttpException("The car is on its way now", HttpStatus.CONFLICT);
    }

    const started = new Date();
    const status = TripStatus.Waiting;
    const trip = await this.tripModel.create({ ...createTripDto, started, status });
    let populatedTrip = await (await trip.populate('car')).populate('car.driver');

    await this.amqpConnection.publish(
      RABBITMQ_TRIP_EXCHANGE,
      RABBITMQ_TRIP_KEY,
      new EventTripDto(populatedTrip)
    );
    return trip;
  }

  async findAll(filter = undefined) {
    const objProperties = ['car'];
    const map = Object.entries(filter).map(([key, value]) => {
      if (objProperties.includes(key)) {
        let conv: string = (value as string);
        return [key, new Types.ObjectId(conv)]
      }
      return [key, value];
    });
    return await this.tripModel.find(Object.fromEntries(map));
  }

  findOne(id: string) {
    return this.tripModel.findById(id);
  }

  async update(id: string, updateTripDto: UpdateTripDto) {
    if (TripsService.activeStatuses().includes(updateTripDto.status)) {
      
      let trip = await this.tripModel.findById(id);
      if ( !trip ) {
        throw new Error(RABBITMQ_ERROR_TRIP_NOT_FOUND);
      }

      let tripsOnWay = await this.tripModel.find({
        _id: {
          $ne: id
        },
        car: trip.get('car'),
        status: {
          $in: TripsService.activeStatuses()
        }
      });
      if (tripsOnWay.length) {
        throw new HttpException("The car is on its way now", HttpStatus.CONFLICT);
      }
    }
    
    if (!TripsService.activeStatuses().includes(updateTripDto.status)
      && !updateTripDto.finished) {
        updateTripDto.finished = new Date();
    }

    return this.tripModel.findByIdAndUpdate(id, updateTripDto);
  }

  remove(id: string) {
    return this.tripModel.findByIdAndRemove(id);
  }

  private static activeStatuses(): TripStatus[] {
    return [TripStatus.Waiting, TripStatus.OnItsWay]
  }
}
