import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { DriverDto } from '../repositories/drivers/dto/driver.dto';
import { Driver, DriverDocument } from '../repositories/drivers/entities/driver.entity';

@Injectable()
export class DriversService {
  constructor(@InjectModel(Driver.name) private carModel: Model<DriverDocument>) {}

  create(createDriverDto: DriverDto) {
    return this.carModel.create(createDriverDto);
  }

  findAll(filter = undefined) {
    return this.carModel.find(filter);
  }

  findOne(id: string) {
    return this.carModel.findById(id);
  }

  update(id: string, updateDriverDto: DriverDto) {
    return this.carModel.findByIdAndUpdate(id, updateDriverDto);
  }

  remove(id: string) {
    return this.carModel.findByIdAndRemove(id);
  }
}
