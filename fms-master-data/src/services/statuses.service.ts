import { Nack, RabbitRPC } from '@golevelup/nestjs-rabbitmq';
import { Injectable } from '@nestjs/common';
import { StatusTripDto } from 'src/repositories/trips/dto/messages/status-trip.dto';
import { UpdateTripDto } from 'src/repositories/trips/dto/update-trip.dto';
import { TripsService } from './trips.service';
import { 
    RABBITMQ_ERROR_TRIP_NOT_FOUND, 
    RABBITMQ_SIMULATOR_EXCHANGE, 
    RABBITMQ_STATUSES_KEY, 
    RABBITMQ_STATUSES_QUEUE 
} from 'src/utils/constants';


@Injectable()
export class StatusesService {
    constructor(private tripsService: TripsService) { }

    @RabbitRPC({
        exchange: RABBITMQ_SIMULATOR_EXCHANGE,
        routingKey: RABBITMQ_STATUSES_KEY,
        queue: RABBITMQ_STATUSES_QUEUE,
    })
    public async rpcHandler( msg: StatusTripDto ) {
        try {
            let updateTrip = new UpdateTripDto;
            updateTrip.status = msg.status;
            await this.tripsService.update(msg.id, updateTrip);
            return new Nack();            
        } catch (error) {
            if ( error.message === RABBITMQ_ERROR_TRIP_NOT_FOUND) {
                return new Nack();
            }
            return new Nack(true);
        }
    }
}