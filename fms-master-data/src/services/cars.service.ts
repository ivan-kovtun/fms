import { Model, Types } from 'mongoose';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CarDto } from '../repositories/cars/dto/car.dto';
import { Car, CarDocument } from '../repositories/cars/entities/car.entity';
import { AssignDto } from '../repositories/cars/dto/assign.dto';

@Injectable()
export class CarsService {
  constructor(@InjectModel(Car.name) private carModel: Model<CarDocument>) { }

  async create(createCarDto: CarDto) {
    return await this.carModel.create(createCarDto);
  }

  async assignCar(id: string, assignCarDto: AssignDto) {
    let assignedCars = await this.carModel.find({ driver: new Types.ObjectId(assignCarDto.driver) });
    if ( assignedCars.length ) {
      throw new HttpException(`The driver is already assigned to car ${[assignedCars[0]._id, assignedCars[0].ref]}`, HttpStatus.CONFLICT);
    }
    /* for(const car of assignedCars) {
      car.driver = null;
      await car.save();
    }; */ 
    return await this.carModel.findByIdAndUpdate(id, assignCarDto);
  }

  async findAll(filter = undefined) {
    const objProperties = ['driver'];
    const map = Object.entries(filter).map(([key, value]) => {
      if (objProperties.includes(key)) {
        let conv : string = (value as string);
        return [key, new Types.ObjectId(conv)]
      }
      return [key, value];
    });
    return await this.carModel.find(Object.fromEntries(map));
  }

  findOne(id: string) {
    return this.carModel.findById(id);
  }

  update(id: string, updateCarDto: CarDto) {
    return this.carModel.findByIdAndUpdate(id, updateCarDto);
  }

  remove(id: string) {
    return this.carModel.findByIdAndRemove(id);
  }
}