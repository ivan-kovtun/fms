import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { DriversModule } from './modules/drivers.module';
import { MONGODB_URI, MONDODB_NAME } from './utils/constants';
import { DriversController } from './controllers/drivers.controller';
import { CarsModule } from './modules/cars.module';
import { TripsModule } from './modules/trips.module';
import { ExceptionsMongoFilter } from './utils/mongo.exception.filter'
import { APP_FILTER } from '@nestjs/core';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        uri: configService.get<string>(MONGODB_URI),
        dbName: configService.get<string>(MONDODB_NAME)
      }),
      inject: [ConfigService]
    }),
    CarsModule,
    DriversModule,
    TripsModule
  ],
  controllers: [DriversController],
  providers: [
    {
      provide: APP_FILTER,
      useClass: ExceptionsMongoFilter,
    }
  ],
})
export class AppModule { }
