export const MONGO_PROVIDER = 'DATABASE_CONNECTION';
export const MONGODB_URI = 'MONGODB_URI';
export const MONDODB_NAME = 'MONDODB_NAME';
export const MONGODB_ERROR = 'MONGODB_ERROR';

export const REST_CONNECTION_PORT = 'REST_CONNECTION_PORT';

export const RABBITMQ_TRIP_EXCHANGE = 'fms.masterData.trip';
export const RABBITMQ_TRIP_EXCHANGE_TYPE = 'direct';
export const RABBITMQ_TRIP_KEY = 'fms.trip.events';
export const RABBITMQ_URI = 'RABBITMQ_URI';
export const RABBITMQ_SIMULATOR_EXCHANGE = 'fms.simulator';
export const RABBITMQ_STATUSES_QUEUE = 'fms.trip.statuses';
export const RABBITMQ_STATUSES_KEY = 'fms.trip.statuses';

export const RABBITMQ_ERROR_TRIP_NOT_FOUND = 'RABBITMQ_ERROR_TRIP_NOT_FOUND';

