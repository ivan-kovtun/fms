
import { Catch, ArgumentsHost, HttpException, HttpStatus } from '@nestjs/common';
import { BaseExceptionFilter } from '@nestjs/core';
import { mongo } from 'mongoose';
 
@Catch()
export class ExceptionsMongoFilter extends BaseExceptionFilter {
  catch(exception: unknown, host: ArgumentsHost) {
    if (exception instanceof mongo.MongoServerError) {
      exception = new HttpException(exception.message, HttpStatus.BAD_REQUEST);
    }
    super.catch(exception, host);
  }
}