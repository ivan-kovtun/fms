import { IsNotEmpty, MinLength, MaxLength, IsString, IsNumber } from 'class-validator';

export class AssignDto {
    @IsNotEmpty()
    @IsString()
    readonly driver: string;
}