import { IsNotEmpty, MinLength, MaxLength, IsString, IsNumber } from 'class-validator';

export class CarDto {
    @IsNotEmpty()
    @IsString()
    ref: string;

    @IsNotEmpty()
    @IsString()
    model: string;

    @IsNotEmpty()
    @IsNumber()
    maxSpeed: number;

    @IsNotEmpty()
    @MinLength(4)
    @MaxLength(4)
    year: string;
}
