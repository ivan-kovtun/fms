import * as mongoose from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Driver } from '../../drivers/entities/driver.entity';

export type CarDocument = mongoose.HydratedDocument<Car>;

@Schema()
export class Car {
    _id: mongoose.ObjectId;

    @Prop({ required: true })
    readonly ref: string;

    @Prop({ required: true })
    readonly model: string;

    @Prop({ required: true })
    readonly maxSpeed: number;

    @Prop({ 
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'Driver'
    })
    driver: Driver;

    @Prop({ required: true })
    readonly year: string;
}

export const CarSchema = SchemaFactory.createForClass(Car);