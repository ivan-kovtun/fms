import { IsNotEmpty, IsString } from 'class-validator';
import { Point, TripStatus } from './trip-helper';

export class CreateTripDto {
    @IsNotEmpty()
    origin: Point;
    
    @IsNotEmpty()
    destination: Point;

    @IsNotEmpty()
    @IsString()
    car: string;
}
