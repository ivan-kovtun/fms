import { PartialType } from '@nestjs/mapped-types';
import { CreateTripDto } from './create-trip.dto';
import { TripStatus } from './trip-helper';

export class UpdateTripDto extends PartialType(CreateTripDto) {
    status: TripStatus;
    started: Date;
    finished: Date;
}
