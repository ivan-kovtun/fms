import { TripStatus } from '../trip-helper';

export class StatusTripDto {
    status: TripStatus;
    id: string;
}