import { CarDto } from 'src/repositories/cars/dto/car.dto';
import { DriverDto } from 'src/repositories/drivers/dto/driver.dto';
import { Point, TripStatus } from '../trip-helper';
import { PartialType } from '@nestjs/mapped-types';

class EventDriver extends PartialType(DriverDto) {
    id: string;
    constructor(driver: any) {
        super();
        this.id = driver._id;
        this.firstName = driver.firstName;
        this.lastName = driver.lastName;
        this.licenseId = driver.licenseId;
        this.risk = driver.risk;
    }
}

class EventCar extends PartialType(CarDto) {
    id: string;
    driver: EventDriver;
    constructor(car: any) {
        super();
        this.id = car._id;
        this.driver = new EventDriver(car.driver);
        this.maxSpeed = car.maxSpeed;
        this.model = car.model;
        this.ref = car.ref;
        this.year = car.year;
    }
}

export class EventTripDto {
    id: string;
    origin: Point;
    destination: Point;
    car: EventCar;
    status: TripStatus;
    started: Date;
    finished: Date;

    constructor(trip: any){
        this.id = trip._id.toString();
        this.status = trip.status;
        this.started = trip.started;
        this.finished = trip.finished;
        this.origin = new Point;
        Object.assign(this.origin, trip.origin);
        this.destination = new Point;
        Object.assign(this.destination, trip.destination);
        this.car = new EventCar(trip.car);
    }
}