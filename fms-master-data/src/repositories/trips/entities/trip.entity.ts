import * as mongoose from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Car } from '../../cars/entities/car.entity';
import { Point, TripStatus } from '../dto/trip-helper';


export type CarDocument = mongoose.HydratedDocument<Car>;



@Schema()
export class Trip {
    _id: mongoose.ObjectId;

    @Prop({ required: true })
    readonly origin: Point;

    @Prop({ required: true })
    readonly destination: Point;

    @Prop({ required: true })
    status: TripStatus;

    @Prop({ 
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'Car'
    })
    readonly car: Car;

    @Prop()
    readonly started: Date;

    @Prop()
    readonly finished: Date;
}

export const TripSchema = SchemaFactory.createForClass(Trip);
