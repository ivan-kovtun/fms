import { IsAlphanumeric, IsNumber, IsDefined } from 'class-validator';
export class DriverDto {

    @IsAlphanumeric()
    @IsDefined()
    firstName: string;

    @IsAlphanumeric()
    @IsDefined()
    lastName: string;

    @IsNumber()
    @IsDefined()
    risk: number;

    @IsDefined()
    licenseId: string;
}
