import * as mongoose from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

export type DriverDocument = mongoose.HydratedDocument<Driver>;

@Schema()
export class Driver {
    _id: mongoose.ObjectId;

    @Prop({ required: true })
    readonly firstName: string;

    @Prop({ required: true })
    readonly lastName: string;

    @Prop({ required: true })
    readonly risk: number;

    @Prop({
        required: true,
        unique: true
    })
    readonly licenseId: string;
}

export const DriverSchema = SchemaFactory.createForClass(Driver);