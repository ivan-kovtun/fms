import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { TripsService } from '../services/trips.service';
import { TripsController } from '../controllers/trips.controller';
import { Trip, TripSchema } from '../repositories/trips/entities/trip.entity';
import { Car, CarSchema } from 'src/repositories/cars/entities/car.entity';
import { CarsService } from 'src/services/cars.service';
import { RabbitMQModule } from '@golevelup/nestjs-rabbitmq';
import { RABBITMQ_TRIP_EXCHANGE, RABBITMQ_TRIP_EXCHANGE_TYPE, RABBITMQ_URI } from '../utils/constants';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { StatusesService } from 'src/services/statuses.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Trip.name, schema: TripSchema },
      { name: Car.name, schema: CarSchema }
    ]),
    ConfigModule,
    RabbitMQModule.forRootAsync(RabbitMQModule, {
      imports: [ConfigModule.forRoot()],
      useFactory: async (configService: ConfigService) => {
        let options = ({
          exchanges: [
            {
              name: RABBITMQ_TRIP_EXCHANGE,
              type: RABBITMQ_TRIP_EXCHANGE_TYPE,
            },
          ],
          uri: configService.get<string>(RABBITMQ_URI),
          connectionInitOptions: { wait: false },
        });
        return options;
      },
      inject: [ConfigService]
    })],
  controllers: [TripsController],
  providers: [TripsService, CarsService, StatusesService]
})
export class TripsModule { }
