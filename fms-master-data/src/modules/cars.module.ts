import { Module } from '@nestjs/common';
import { CarsService } from '../services/cars.service';
import { CarsController } from '../controllers/cars.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Car, CarSchema } from '../repositories/cars/entities/car.entity';
import { Driver, DriverSchema } from 'src/repositories/drivers/entities/driver.entity';

@Module({
  imports: [MongooseModule.forFeature([{ name: Car.name, schema: CarSchema },
                                      { name: Driver.name, schema: DriverSchema }])],
  controllers: [CarsController],
  providers: [CarsService],
  exports: [CarsService]
})
export class CarsModule {}
