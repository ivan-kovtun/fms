import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { DriversService } from '../services/drivers.service';
import { DriversController } from '../controllers/drivers.controller';
import { Driver, DriverSchema } from '../repositories/drivers/entities/driver.entity';

@Module({
  imports: [MongooseModule.forFeature([{ name: Driver.name, schema: DriverSchema }])],
  controllers: [DriversController],
  providers: [DriversService],
  exports: [DriversService]
})
export class DriversModule {}
