import { Controller, Get, Post, Body, Patch, Param, Delete, ValidationPipe, Request } from '@nestjs/common';
import { Catch } from '@nestjs/common/decorators';
import { CarsService } from '../services/cars.service';
import { AssignDto } from '../repositories/cars/dto/assign.dto';
import { CarDto } from '../repositories/cars/dto/car.dto';
import { urlParamsToFilter } from '../utils/urlParamsToFilter';

@Controller('cars')
export class CarsController {
  constructor(private readonly carsService: CarsService) {}

  @Post()
  async create(@Body(new ValidationPipe()) createCarDto: CarDto) {
      return await this.carsService.create(createCarDto);
  }

  @Post(':id/assign')
  async assign(@Param('id') id: string, @Body(new ValidationPipe()) assignCarDto: AssignDto) {
      return await this.carsService.assignCar(id, assignCarDto);
  }

  @Get()
  findAll(@Request() req) {
    let filter = urlParamsToFilter(req);
    return this.carsService.findAll(filter);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.carsService.findOne(id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body(new ValidationPipe()) updateCarDto: CarDto) {
    return this.carsService.update(id, updateCarDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.carsService.remove(id);
  }

}


