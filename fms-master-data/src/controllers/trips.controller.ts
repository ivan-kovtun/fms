import { Controller, Get, Post, Body, Patch, Param, Delete, Request } from '@nestjs/common';
import { TripsService } from '../services/trips.service';
import { CreateTripDto } from '../repositories/trips/dto/create-trip.dto';
import { UpdateTripDto } from '../repositories/trips/dto/update-trip.dto';
import { urlParamsToFilter } from 'src/utils/urlParamsToFilter';

@Controller('trips')
export class TripsController {
  constructor(private readonly tripsService: TripsService) {}

  @Post()
  create(@Body() createTripDto: CreateTripDto) {
    return this.tripsService.create(createTripDto);
  }

  @Get()
  findAll(@Request() req) {
    let filter = urlParamsToFilter(req);
    return this.tripsService.findAll(filter);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.tripsService.findOne(id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateTripDto: UpdateTripDto) {
    return this.tripsService.update(id, updateTripDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.tripsService.remove(id);
  }
}
