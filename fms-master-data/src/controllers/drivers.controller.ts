import { Controller, Get, Post, Body, Patch, Param, Delete, Request } from '@nestjs/common';
import { urlParamsToFilter } from 'src/utils/urlParamsToFilter';
import { DriversService } from '../services/drivers.service';
import { DriverDto } from '../repositories/drivers/dto/driver.dto';

@Controller('drivers')
export class DriversController {
  constructor(private readonly driversService: DriversService) {}

  @Post()
  create(@Body() createDriverDto: DriverDto) {
    return this.driversService.create(createDriverDto);
  }

  @Get()
  findAll(@Request() req) {
    let filter = urlParamsToFilter(req);
    return this.driversService.findAll(filter);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.driversService.findOne(id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateDriverDto: DriverDto) {
    return this.driversService.update(id, updateDriverDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.driversService.remove(id);
  }
}
